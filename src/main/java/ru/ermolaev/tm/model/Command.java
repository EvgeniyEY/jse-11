package ru.ermolaev.tm.model;

import ru.ermolaev.tm.constant.ICommandConst;
import ru.ermolaev.tm.constant.IArgumentConst;

public enum Command implements ICommandConst {

    HELP(ICommandConst.HELP, IArgumentConst.HELP, "Display terminal commands."),
    ABOUT(ICommandConst.ABOUT, IArgumentConst.ABOUT, "Show developer info."),
    VERSION(ICommandConst.VERSION, IArgumentConst.VERSION, "Show version info."),
    INFO(ICommandConst.INFO, IArgumentConst.INFO, "Show hardware info."),
    EXIT(ICommandConst.EXIT, null, "Exit from application."),
    COMMANDS(ICommandConst.COMMANDS, IArgumentConst.COMMANDS, "Show application's commands."),
    ARGUMENTS(ICommandConst.ARGUMENTS, IArgumentConst.ARGUMENTS, "Show application's arguments."),
    TASK_CREATE(ICommandConst.TASK_CREATE, null, "Create a new task."),
    TASK_DELETE(ICommandConst.TASK_DELETE, null, "Delete all task."),
    TASK_LIST(ICommandConst.TASK_LIST, null, "Show task list."),
    PROJECT_CREATE(ICommandConst.PROJECT_CREATE, null, "Create a new project."),
    PROJECT_DELETE(ICommandConst.PROJECT_DELETE, null, "Delete all projects."),
    PROJECT_LIST(ICommandConst.PROJECT_LIST, null, "Show project list.");

    private String command;

    private String argument;

    private String description;

    Command(final String command, final String argument, final  String description) {
        this.command = command;
        this.argument = argument;
        this.description = description;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        if (command != null && !command.isEmpty()) result.append(command);
        if (argument != null && !argument.isEmpty()) result.append(", ").append(argument);
        if (description != null && !description.isEmpty()) result.append(": ").append(description);
        return result.toString();
    }

}
