package ru.ermolaev.tm.bootstrap;

import ru.ermolaev.tm.api.controller.ICommandController;
import ru.ermolaev.tm.api.controller.IProjectController;
import ru.ermolaev.tm.api.controller.ITaskController;
import ru.ermolaev.tm.api.repository.ICommandRepository;
import ru.ermolaev.tm.api.repository.IProjectRepository;
import ru.ermolaev.tm.api.repository.ITaskRepository;
import ru.ermolaev.tm.api.service.ICommandService;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.constant.IArgumentConst;
import ru.ermolaev.tm.constant.ICommandConst;
import ru.ermolaev.tm.controller.CommandController;
import ru.ermolaev.tm.controller.ProjectController;
import ru.ermolaev.tm.controller.TaskController;
import ru.ermolaev.tm.repository.CommandRepository;
import ru.ermolaev.tm.repository.ProjectRepository;
import ru.ermolaev.tm.repository.TaskRepository;
import ru.ermolaev.tm.service.CommandService;
import ru.ermolaev.tm.service.ProjectService;
import ru.ermolaev.tm.service.TaskService;
import ru.ermolaev.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    public void run(final String[] args) {
        System.out.println("Welcome to task manager");
        if (parseArgs(args)) System.exit(0);
        while (true) parseCommand(TerminalUtil.nextLine());
    }

    public boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseArg(final String arg) {
        if (arg.isEmpty()) return;
        switch (arg) {
            case IArgumentConst.HELP:
                commandController.showHelp();
                break;
            case IArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case IArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case IArgumentConst.INFO:
                commandController.showInfo();
                break;
            case IArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case IArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                System.out.println("Invalid argument");
        }
    }

    public void parseCommand(final String arg) {
        if (arg.isEmpty()) return;
        switch (arg) {
            case ICommandConst.HELP:
                commandController.showHelp();
                break;
            case ICommandConst.ABOUT:
                commandController.showAbout();
                break;
            case ICommandConst.VERSION:
                commandController.showVersion();
                break;
            case ICommandConst.INFO:
                commandController.showInfo();
                break;
            case ICommandConst.EXIT:
                commandController.exit();
            case ICommandConst.COMMANDS:
                commandController.showCommands();
                break;
            case ICommandConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ICommandConst.TASK_CREATE:
                taskController.createTask();
                break;
            case ICommandConst.TASK_DELETE:
                taskController.deleteTasks();
                break;
            case ICommandConst.TASK_LIST:
                taskController.showTasks();
                break;
            case ICommandConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case ICommandConst.PROJECT_DELETE:
                projectController.deleteProjects();
                break;
            case ICommandConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            default:
                System.out.println("Invalid command");
        }
    }

}
