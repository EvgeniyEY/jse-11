package ru.ermolaev.tm.api.controller;

public interface ICommandController {

    void showHelp();

    void showCommands();

    void showArguments();

    void showAbout();

    void showVersion();

    void showInfo();

    void exit();

}
