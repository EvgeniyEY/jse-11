package ru.ermolaev.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void deleteProjects();

    void createProject();

}
