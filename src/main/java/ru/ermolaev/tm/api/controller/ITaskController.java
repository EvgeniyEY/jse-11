package ru.ermolaev.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void deleteTasks();

    void createTask();

}
