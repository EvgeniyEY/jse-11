package ru.ermolaev.tm.api.service;

import ru.ermolaev.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void createProject(String name);

    void createProject(String name, String description);

    void addProject(Project project);

    List<Project> showAllProjects();

    void deleteProject(Project project);

    void deleteAllProjects();

}
