package ru.ermolaev.tm.api.service;

import ru.ermolaev.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}
