package ru.ermolaev.tm.api.service;

import ru.ermolaev.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void createTask(String name);

    void createTask(String name, String description);

    void addTask(Task task);

    List<Task> showAllTasks();

    void deleteTask(final Task task);

    void deleteAllTasks();

}
