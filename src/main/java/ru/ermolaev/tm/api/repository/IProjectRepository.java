package ru.ermolaev.tm.api.repository;

import ru.ermolaev.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    void addProject(Project project);

    List<Project> showAllProjects();

    void deleteProject(Project project);

    void deleteAllProjects();

}
