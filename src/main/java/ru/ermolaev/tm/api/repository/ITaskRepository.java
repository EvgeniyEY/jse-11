package ru.ermolaev.tm.api.repository;

import ru.ermolaev.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void addTask(Task task);

    List<Task> showAllTasks();

    void deleteTask(Task task);

    void deleteAllTasks();

}
