package ru.ermolaev.tm.controller;

import ru.ermolaev.tm.api.controller.IProjectController;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.model.Project;
import ru.ermolaev.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[PROJECT LIST]");
        final List<Project> projects = projectService.showAllProjects();
        for (Project project: projects) System.out.println(project);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void deleteProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.deleteAllProjects();
        System.out.println("[COMPLETE]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        projectService.createProject(name, description);
        System.out.println("[COMPLETE]");
    }
    
}
