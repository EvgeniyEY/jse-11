package ru.ermolaev.tm.controller;

import ru.ermolaev.tm.api.controller.ITaskController;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.model.Task;
import ru.ermolaev.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[TASK LIST]");
        final List<Task> tasks = taskService.showAllTasks();
        for (Task task: tasks) System.out.println(task);
        System.out.println("[COMPLETE]");
    }

    @Override
    public void deleteTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.deleteAllTasks();
        System.out.println("[COMPLETE]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        taskService.createTask(name, description);
        System.out.println("[COMPLETE]");
    }

}
