package ru.ermolaev.tm.constant;

public interface IArgumentConst {

    String HELP = "-h";

    String ABOUT = "-a";

    String VERSION = "-v";

    String INFO = "-i";

    String COMMANDS = "-cmd";

    String ARGUMENTS = "-arg";

}
