package ru.ermolaev.tm.constant;

public interface ICommandConst {

    String HELP = "help";

    String ABOUT = "about";

    String VERSION = "version";

    String INFO = "info";

    String EXIT = "exit";

    String COMMANDS = "commands";

    String ARGUMENTS = "arguments";

    String TASK_CREATE = "task-create";

    String TASK_DELETE = "task-delete";

    String TASK_LIST = "task-list";

    String PROJECT_CREATE = "project-create";

    String PROJECT_DELETE = "project-delete";

    String PROJECT_LIST = "project-list";

}
