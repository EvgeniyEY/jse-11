package ru.ermolaev.tm.repository;

import ru.ermolaev.tm.api.repository.IProjectRepository;
import ru.ermolaev.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private List<Project> projects = new ArrayList<>();

    @Override
    public void addProject(Project project) {
        projects.add(project);
    }

    @Override
    public List<Project> showAllProjects() {
        return projects;
    }

    @Override
    public void deleteProject(Project project) {
        projects.remove(project);
    }

    @Override
    public void deleteAllProjects() {
        projects.clear();
    }

}
