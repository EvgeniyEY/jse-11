package ru.ermolaev.tm.repository;

import ru.ermolaev.tm.api.repository.ITaskRepository;
import ru.ermolaev.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private List<Task> tasks = new ArrayList<>();

    @Override
    public void addTask(Task task) {
        tasks.add(task);
    }

    @Override
    public List<Task> showAllTasks() {
        return tasks;
    }

    @Override
    public void deleteTask(Task task) {
        tasks.remove(task);
    }

    @Override
    public void deleteAllTasks() {
        tasks.clear();
    }

}
