package ru.ermolaev.tm.repository;

import ru.ermolaev.tm.api.repository.ICommandRepository;
import ru.ermolaev.tm.model.Command;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            Command.HELP,
            Command.ABOUT,
            Command.VERSION,
            Command.INFO,
            Command.COMMANDS,
            Command.ARGUMENTS,
            Command.TASK_CREATE,
            Command.TASK_DELETE,
            Command.TASK_LIST,
            Command.PROJECT_CREATE,
            Command.PROJECT_DELETE,
            Command.PROJECT_LIST,
            Command.EXIT
    };

    private final String[] COMMANDS = getCommands(TERMINAL_COMMANDS);

    private final String[] ARGS = getArgs(TERMINAL_COMMANDS);

    public String[] getCommands(Command... values) {
        if (values == null || values.length == 0) return new String[] {};
        final String[] result = new String[values.length];
        int index = 0;
        for (Command value : values) {
            final String name = value.getCommand();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArgs(Command... values) {
        if (values == null || values.length == 0) return new String[] {};
        final String[] result = new String[values.length];
        int index = 0;
        for (Command value : values) {
            final String arg = value.getArgument();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    public String[] getCommands() {
        return COMMANDS;
    }

    public String[] getArgs() {
        return ARGS;
    }

}
