package ru.ermolaev.tm.service;

import ru.ermolaev.tm.api.repository.IProjectRepository;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void createProject(final String name) {
        final Project project = new Project();
        if (name != null && !name.isEmpty()) project.setName(name);
        projectRepository.addProject(project);
    }

    @Override
    public void createProject(final String name, final String description) {
        final Project project = new Project();
        if (name != null && !name.isEmpty()) project.setName(name);
        if (description != null && !description.isEmpty()) project.setDescription(description);
        projectRepository.addProject(project);
    }

    @Override
    public void addProject(final Project project) {
        if (project == null) return;
        projectRepository.addProject(project);
    }

    @Override
    public List<Project> showAllProjects() {
        return projectRepository.showAllProjects();
    }

    @Override
    public void deleteProject(final Project project) {
        projectRepository.deleteProject(project);
    }

    @Override
    public void deleteAllProjects() {
        projectRepository.deleteAllProjects();
    }

}
