package ru.ermolaev.tm.service;

import ru.ermolaev.tm.api.repository.ICommandRepository;
import ru.ermolaev.tm.api.service.ICommandService;
import ru.ermolaev.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    public String[] getArgs() {
        return commandRepository.getArgs();
    }

}
