package ru.ermolaev.tm.service;

import ru.ermolaev.tm.api.repository.ITaskRepository;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void createTask(final String name) {
        final Task task = new Task();
        if (name != null && !name.isEmpty()) task.setName(name);
        taskRepository.addTask(task);
    }

    @Override
    public void createTask(final String name, final String description) {
        final Task task = new Task();
        if (name != null && !name.isEmpty()) task.setName(name);
        if (description != null && !description.isEmpty()) task.setDescription(description);
        taskRepository.addTask(task);
    }

    @Override
    public void addTask(final Task task) {
        if (task == null) return;
        taskRepository.addTask(task);
    }

    @Override
    public List<Task> showAllTasks() {
        return taskRepository.showAllTasks();
    }

    @Override
    public void deleteTask(final Task task) {
        taskRepository.deleteTask(task);
    }

    @Override
    public void deleteAllTasks() {
        taskRepository.deleteAllTasks();
    }

}
